#include "UDPServer.h"
#include <WS2tcpip.h>
#include <exception>

#pragma comment(lib, "ws2_32")

unsigned short UDPServer::ReceiveBufferSize = 65000; 

UDPServer::UDPServer(void) : SizeOfBuffer(ReceiveBufferSize)
{
    wsaInit = false;
    sockd = NULL;
    wsadata = new WSAData;
    if (WSAStartup(0x202, wsadata))
        throw std::exception("UDPServer() : Can't init server");
    wsaInit = true;
    Buffer = new char[SizeOfBuffer];
    isOpen = false;
    InitSocket();
}
UDPServer::~UDPServer(void)
{
    delete [] Buffer;
    if(sockd != NULL)
        closesocket(sockd);
    if(wsaInit)
        WSACleanup();
    delete wsadata;
}
bool UDPServer::InitSocket()
{
    if(sockd != NULL)
        return true;
    const char opt = 1;
    sockd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    setsockopt(sockd, SOL_SOCKET, SO_BROADCAST, &opt, sizeof(char));
    if (sockd == INVALID_SOCKET)
        return false;

    return true;
}

bool UDPServer::StartServer()
{
    if(InitSocket() == false)
        return false;

    sockaddr_in local_addr;

    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = htonl(IpAddr);
    local_addr.sin_port = htons(Port);
    int retbind = bind(sockd, reinterpret_cast<const sockaddr*>(&local_addr), sizeof(local_addr));
    if (retbind)
        return false;
    
    isOpen = true;
    return true;

}
sockaddr_in UDPServer::GetSockAddr(std::string const &DstAddr, unsigned short DstPort) const
{
    sockaddr_in ret_addr;
    ret_addr.sin_family = AF_INET;
    ret_addr.sin_addr.s_addr = inet_addr(GetHostByName(DstAddr).c_str());
    ret_addr.sin_port = htons(DstPort);    
    return ret_addr;
}
int UDPServer::SendMsg(std::string const &msg, sockaddr_in const &DstAddr) const
{
    int ret = sendto(sockd,
                msg.c_str(),
                static_cast<int>(msg.size()),
                0,
                reinterpret_cast<const sockaddr*>(&DstAddr),
                sizeof(DstAddr));
    if(ret == SOCKET_ERROR)
        throw std::exception("UDPServer | Send error");

    return ret;
    
}
std::string UDPServer::ReceiveMsg(sockaddr_in &client_addr) const
{
    int client_addr_size = sizeof(client_addr);
    int bsize = recvfrom(sockd, 
                        Buffer,
                        SizeOfBuffer,
                        0,
                        (sockaddr *) &client_addr,
                        &client_addr_size);
    if(bsize == SOCKET_ERROR)
        throw std::exception("UDPServer | Receive error");

    return std::string(Buffer, &Buffer[bsize]);
}

bool UDPServer::Start(std::string const &addr, unsigned short port)
{
    IpAddr = inet_addr(addr.c_str());
    Port = port;
    return StartServer();
}
bool UDPServer::Start(unsigned short port)
{
    IpAddr = INADDR_ANY;
    Port = port;
    return StartServer();
}
bool UDPServer::Stop(void)
{
    closesocket(sockd);
    sockd = NULL;
    isOpen = false;
    return true;
}

int UDPServer::Send(std::string const &msg, std::string const &addr, unsigned short port) const
{
    return SendMsg(msg, GetSockAddr(addr, port));
}
int UDPServer::SendBroadCast(std::string const &msg, unsigned short port) const
{
    return SendMsg(msg, GetSockAddr("255.255.255.255", port));
}

std::string UDPServer::Receive() const
{
    sockaddr_in client_addr;
    return ReceiveMsg(client_addr);
}
std::string UDPServer::Receive(__out std::string &ip) const
{
    sockaddr_in client_addr;
    std::string ret = ReceiveMsg(client_addr);
    ip = inet_ntoa(client_addr.sin_addr);
    return ret;
}

int UDPServer::GetLastError() const
{ 
    return WSAGetLastError();
}

std::string UDPServer::GetLocalIpAddr()
{
    WSADATA wsa;
    std::string ret;
    in_addr ret_addr = {0};
    if (WSAStartup(0x202, &wsa))
        return ret;
    
    char hostName[16];
    gethostname(hostName, 16);
    hostent* localHost = gethostbyname(hostName);
    ret_addr = (*(struct in_addr *)localHost->h_addr_list[0]);
    WSACleanup();
    ret = inet_ntoa(ret_addr);
    return ret;
}
std::string UDPServer::GetHostByName(std::string const &addr)
{
    WSADATA wsa;
    std::string ret;
    in_addr ret_addr = {0};
    if (WSAStartup(0x202, &wsa))
        return ret;
    hostent* localHost;
    if(localHost = gethostbyname(addr.c_str()))
    {
        ret_addr = (*(struct in_addr *)localHost->h_addr);
    }
    WSACleanup();
    ret = inet_ntoa(ret_addr);
    return ret;
}
