#pragma once
#include <BaseTsd.h>
#include <string>

struct WSAData;
struct sockaddr_in;
struct in_addr;

class UDPServer
{
public:
    static unsigned short ReceiveBufferSize; 
private:
    WSAData * wsadata;
    UINT_PTR sockd;

    unsigned long IpAddr;
    unsigned short Port;

    const unsigned short SizeOfBuffer;
    mutable char * Buffer;
    bool wsaInit;
    bool isOpen;

    bool InitSocket();
    bool StartServer();

    sockaddr_in GetSockAddr(std::string const &addr, unsigned short port) const;

    int SendMsg(std::string const &msg, sockaddr_in const &DstAddr) const;
    std::string ReceiveMsg(__out sockaddr_in &client_addr) const;

protected:
    UDPServer(const UDPServer& root);
    UDPServer& operator=(const UDPServer&);
public:
    UDPServer(void);
    ~UDPServer(void);

    bool Start(std::string const &addr, unsigned short port);
    bool Start(unsigned short port);
    bool Stop(void);

    int Send(std::string const &msg, std::string const &addr, unsigned short port) const;
    int SendBroadCast(std::string const &msg, unsigned short port) const;

    std::string Receive() const;
    std::string Receive(__out std::string &ip) const;

    int GetLastError() const;

    static std::string GetLocalIpAddr();
    static std::string GetHostByName(std::string const &);
};
